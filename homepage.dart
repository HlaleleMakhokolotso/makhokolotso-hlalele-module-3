import 'package:flutter/material.dart';
import 'profile.dart';

class dashboardScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white10,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Readers Club'),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.popUntil(
            context,
            ModalRoute.withName(Navigator.defaultRouteName),
          );
        },
        child: Icon(Icons.logout_rounded),
      ),
      bottomNavigationBar: BottomAppBar(
        color: Colors.redAccent,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            IconButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => profile()));
              },
              icon: Icon(
                Icons.face_rounded,
                size: 40.0,
              ),
            ),
            IconButton(
              onPressed: () {},
              icon: Icon(
                Icons.search,
                size: 40.0,
              ),
            ),
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Center(
            child: Text(
              'Welcome to Readers Club',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 4, 119, 8),
                  fontSize: 30),
            ),
          ),
          Center(
            child: Text(
              'get your book or join the club discusion below',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Color.fromARGB(255, 66, 221, 71),
                  fontSize: 15),
            ),
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 150,
                  width: 150,
                  padding: EdgeInsets.all(0.8),
                  decoration: BoxDecoration(
                      color: Colors.tealAccent, shape: BoxShape.circle),
                  child: ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(primary: Colors.blue),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => books()));
                    },
                    label: Text('Get Book'),
                    icon: Icon(Icons.book),
                  ),
                ),
                Container(
                  color: Colors.tealAccent,
                  height: 150,
                  width: 150,
                  padding: EdgeInsets.all(0.8),
                  child: Center(
                    child: Text(
                      'Get to the book room and start reading your favorite book',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 1, 99, 4),
                          fontSize: 10),
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                Container(
                  height: 150,
                  width: 150,
                  padding: EdgeInsets.all(0.8),
                  decoration: BoxDecoration(
                      color: Colors.tealAccent, shape: BoxShape.circle),
                  child: ElevatedButton.icon(
                    style: ElevatedButton.styleFrom(primary: Colors.blue),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => club()));
                    },
                    label: Text('Join club'),
                    icon: Icon(Icons.people),
                  ),
                ),
                Container(
                  color: Colors.tealAccent,
                  height: 150,
                  width: 150,
                  padding: EdgeInsets.all(0.8),
                  child: Center(
                    child: Text(
                      'For amazing book discussions and discusion meet up join the Club',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Color.fromARGB(255, 1, 99, 4),
                          fontSize: 10),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class books extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Reading Club Library'),
      ),
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              width: 400,
              height: 500,
              color: Colors.tealAccent,
              child: Text(
                'Welcome to the first Reading Club Screen',
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class club extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Reading Club Community'),
      ),
      body: Column(
        //mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: Container(
              width: 400,
              height: 500,
              color: Colors.tealAccent,
              child: Text(
                'Welcome to the second Reading Club Screen, you can press back button navigate to previous screen',
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 25,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
