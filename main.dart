import 'package:flutter/material.dart';
import 'homepage.dart';

void main() => runApp(App());

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: loginScreen(),
    );
  }
}

class loginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.tealAccent,
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            CircleAvatar(
              radius: 50.0,
              //backgroundColor: Colors.redAccent,
              backgroundImage: AssetImage('assets/hlalele.jpg'),
              //child: Icon(Icons.face_rounded, size: 50),
            ),
            Text(
              'Welcome back!',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.white,
                  fontSize: 35),
            ),
            Container(
              color: Colors.white,
              width: 300.0,
              child: TextField(
                decoration: InputDecoration(
                    hintText: 'hlalelem@blankdevs.co.za',
                    icon: Icon(Icons.email, color: Colors.redAccent),
                    labelText: 'Email',
                    border: OutlineInputBorder()),
              ),
            ),
            Container(
              color: Colors.white,
              width: 300.0,
              child: TextField(
                obscureText: true,
                decoration: InputDecoration(
                    icon: Icon(Icons.security, color: Colors.redAccent),
                    labelText: 'Password',
                    border: OutlineInputBorder()),
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            ElevatedButton(
              style: ElevatedButton.styleFrom(
                shape: StadiumBorder(),
                primary: Colors.red,
              ),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => dashboardScreen()));
              },
              child: Text(
                'Login!',
                style: TextStyle(fontSize: 32.0, color: Colors.blue),
              ),
            ),
            SizedBox(
              height: 5.0,
            ),
            Center(
              child: ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.tealAccent,
                ),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => registrationScreen()));
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text("Don't have an account?"),
                    Text('Sign up',
                        style:
                            TextStyle(color: Color.fromARGB(255, 146, 3, 122))),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class registrationScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(centerTitle: true, title: Text('Registration Screen')),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Container(
            height: 100,
            child: Text(
              'Create your account',
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                  fontSize: 30),
            ),
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  color: Colors.tealAccent,
                  width: 200.0,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Full Name',
                    ),
                  ),
                ),
                Container(
                  color: Colors.tealAccent,
                  width: 200.0,
                  child: TextField(
                    decoration: InputDecoration(
                      labelText: 'Email',
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10.0,
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  color: Colors.tealAccent,
                  width: 200.0,
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Password',
                    ),
                  ),
                ),
                Container(
                  color: Colors.tealAccent,
                  width: 200.0,
                  child: TextField(
                    obscureText: true,
                    decoration: InputDecoration(
                      labelText: 'Confirm Password',
                    ),
                  ),
                ),
              ],
            ),
          ),
          SizedBox(
            height: 50.0,
          ),
          ElevatedButton(
            style: ElevatedButton.styleFrom(
              shape: StadiumBorder(),
              primary: Colors.tealAccent,
            ),
            onPressed: () {
              Navigator.pop(context);
            },
            child: Text(
              'Register!',
              style: TextStyle(
                  fontSize: 32.0, color: Color.fromARGB(255, 6, 102, 9)),
            ),
          ),
        ],
      ),
    );
  }
}
