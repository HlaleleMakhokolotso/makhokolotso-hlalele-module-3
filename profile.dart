import 'dart:ui';

import 'package:flutter/material.dart';

class profile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      appBar: AppBar(
        centerTitle: true,
        title: Text('Edit Profile'),
      ),
      body: Column(
        children: [
          Center(
            child: CircleAvatar(
              radius: 50.0,
              //backgroundColor: Colors.redAccent,
              backgroundImage: AssetImage('assets/hlalele.jpg'),
              //child: Icon(Icons.face_rounded, size: 50),
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: Row(
              children: [
                SizedBox(
                  width: 10.0,
                ),
                Text('Hlalele Makhokolotso'),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: Row(
              children: [
                SizedBox(
                  width: 10.0,
                ),
                Text('hlalelem@blankdevs.co.za'),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: Row(
              children: [
                SizedBox(
                  width: 10.0,
                ),
                Text('************'),
              ],
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            color: Colors.white,
            margin: EdgeInsets.symmetric(vertical: 10.0, horizontal: 25.0),
            child: Row(
              children: [
                SizedBox(
                  width: 10.0,
                ),
                Text('************'),
              ],
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ButtonTheme(
                minWidth: 80.0,
                child: ElevatedButton(
                  onPressed: () {
                    Navigator.pop(context);
                  },
                  child: Text(
                    'Save',
                    style: TextStyle(color: Color.fromARGB(255, 6, 102, 9)),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
